package pl.pawelzadykowicz.pb01_02;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;

public class MainActivity extends Activity {

	/**
	 * Wątek roboczy - aktualizujący GUI i "wykonujący pracę". należy zrobić
	 * tak, by nie tworzyć go za każdym razem, gdy tworzone jest activity (wątek
	 * jest "ciężkim i kosztownym" obiektem).
	 * 
	 * @author zadykowiczp
	 * 
	 */
	private static final class WorkerThread extends Thread {
		/**
		 * referencja na activity, które aktualizujemy.
		 */
		private MainActivity listener;

		/**
		 * 
		 * @param listener
		 *            aktualizowane activity.
		 */
		public synchronized void setListener(MainActivity listener) {
			this.listener = listener;
		}

		@Override
		public void run() {

			// wątek pozostaje żywy praktycznie na zawsze (quit, nie wywołamy).
			while (true) {

				synchronized (this) {
					// wątek zatrzymuje się na wait, jeśli nie mamy referencji
					// na Activity
					// lub jeśli Activity nie jest gotowe (ustawiamy ready w
					// onCreate()).
					// nie jest wtedy widoczne, ale w tym przykładzie chcemy,
					// aby postęp miał miejsce także, gdy activity nie jest
					// widoczne - symulacja pracy z zewnętrznym zasobem.

					while (isListenerNotReady() || isMaxPositionReached()) {
						if (isListenerQuiting()) {
							return;
						}
						try {
							wait();
						} catch (InterruptedException e) {
							this.interrupt();
							return;
						}
					}

					// aktualizacja GUI.
					if (listener != null) {
						listener.tick();
					}

				}

				// krótka pauza - tak, jakbyśmy robili w tym wątku np.:
				// komunikację ze zdalnym zasobem.
				synchronized (this) {
					try {
						if (isInterrupted()) {
							return;
						}
						if (isListenerQuiting()) {
							return;
						}
						wait(50);
					} catch (InterruptedException e) {
						this.interrupt();
						return;
					}
				}
			}
		}

		private boolean isListenerQuiting() {
			if (listener == null) {
				return false;
			}
			return listener.isQuiting();
		}

		private boolean isListenerNotReady() {
			if (listener == null) {
				return true;
			}
			return !listener.isReady();
		}

		private boolean isMaxPositionReached() {
			if (listener == null) {
				return true;
			}
			return listener.isMaxReached();
		}
	}

	// inicjujemy zmienną czymkolwiek - w tick() pobierzemy prawdziwą wartość.
	int max = 10000;
	/**
	 * referencja na kontrolkę paska postępu.
	 */
	private ProgressBar progressBar;
	/**
	 * aktualna pozycja do ustawienia w pasku postępu .
	 */
	private int position;
	/**
	 * czy jesteśmy gotowi .
	 */
	private boolean ready = false;
	// jakbyśmy chcieli kulturalnie zakończyć pracę naszego wątku workera, to
	// ustawilibysmy tu true (np: na zakończoną pracę, na przycisk "przerwij"
	// itp).
	private boolean quiting = false;

	/**
	 * wątek pracuje w pętli, a jak dojdzie do końca, to grzecznie czeka na
	 * zresetowanie pozycji.
	 */
	private WorkerThread progressThread = null;

	private static final String TAG = MainActivity.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		progressBar = (ProgressBar) findViewById(R.id.progress_horizontal);
		// listener na kliknięcie w przycisk "restart".
		Button button = (Button) findViewById(R.id.restart);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				restart();
			}
		});

		ready = true;
		progressThread = (WorkerThread) getLastNonConfigurationInstance();
		if (progressThread == null)
		{
			progressThread = new WorkerThread();
			progressThread.start();
		}
		synchronized (progressThread) {
			progressThread.setListener(this);
			progressThread.notify();
		}

	}

	public boolean isQuiting() {
		return quiting;
	}

	public boolean isReady() {
		return ready;
	}

	public boolean isMaxReached() {
		return position >= max;
	}

	/**
	 * krok postępu pracy w GUI.
	 */
	public void tick() {
		position++;
		max = progressBar.getMax();
		progressBar.setProgress(position);
	}

	private void restart() {
		synchronized (progressThread) {
			position = 0;
			progressThread.notify();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		/**
		 * usuwamy swoją referencję z wątku - w przeciwnym wypadku wątek jako GC
		 * root powstrzymałby garbage collector, przed usunięciem martwego
		 * activity.
		 */
		progressThread.setListener(null);
		quiting = true;
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		position = savedInstanceState.getInt(TAG);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(TAG, position);
	}
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		return progressThread;
	}

}
